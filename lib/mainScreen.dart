import 'myForm.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: new AppBar(title: new Text('Flutter.su - Форма ввода')),
      body: new Stack(children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: Image.asset('res/bg 1.jpg').image, fit: BoxFit.cover),
          ),
        ),
        MyForm()
      ]),
    );
  }
}
