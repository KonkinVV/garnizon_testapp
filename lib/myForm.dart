import 'package:flutter/material.dart';
import 'package:garnizon/myTextField.dart';
import 'SecondScreen.dart';

class MyForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyFormState();
}

class MyFormState extends State {
  TextEditingController familyTextController = TextEditingController();
  TextEditingController nameTextController = TextEditingController();
  TextEditingController pinTextController = TextEditingController();
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(vertical :10.0,horizontal: 50.0),
        height: double.infinity,

        child: new SingleChildScrollView(
            child: new Form(
          child: new Column(
            children: <Widget>[
              SizedBox(height: 40,),
              new Image.asset('res/logo1.png'),
              SizedBox(
                height: 40,
              ),
              new Text('Введите имя и фамилию:',
                  style: TextStyle(fontSize: 14.0, color: Color(0xFFFFFFFF))),
              new SizedBox(height: 20.0),
              new MyTextField('Имя', nameTextController),
              new SizedBox(height: 20.0),
              new MyTextField('Фамилия', familyTextController),
              new SizedBox(height: 40.0),
              new Text('Придумайте пин-код',
                  style: TextStyle(fontSize: 14.0, color: Color(0xFFFFFFFF))),
              new SizedBox(height: 20.0),
              new ConstrainedBox(
                  constraints: BoxConstraints(maxWidth: 200),
                  child: new MyTextField('0000', pinTextController)),
              new SizedBox(height: 30.0),
              ClipRRect(
                  child: GestureDetector(
                onTap: () {
                  String fam = familyTextController.text;
                  String name = nameTextController.text;
                  String pin = pinTextController.text;
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SecondScreen(
                              famText: fam, nameText: name, pinText: pin)));
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 33.0,
                  width: 194.0,
                  padding: const EdgeInsets.all(0.0),
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      gradient: LinearGradient(colors: <Color>[
                        Color(0xFFA8070A),
                        Color(0xFF55080A),
                      ])),
                  child: Text(
                    'Получить QR код',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 14.0, color: Colors.white),
                  ),
                ),
              )),
            ],
          ),
        )));
  }
}
