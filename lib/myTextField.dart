import 'package:flutter/material.dart';

class MyTextField extends StatefulWidget {
  final TextEditingController _textController;
  final String _hintText;

  MyTextField(this._hintText, this._textController);

  @override
  createState() => new MyTextFieldState(this._hintText, this._textController);
}

class MyTextFieldState extends State<MyTextField> {
  TextEditingController textController;
  String hintText;

  MyTextFieldState(this.hintText, this.textController);

  @override
  Widget build(BuildContext context) {
    return new ClipRRect(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(100.0)),
          ),
          child: TextField(
            controller: this.textController,
            obscureText: false,
            textAlign: TextAlign.center,
            cursorColor: Colors.white,
            decoration: InputDecoration(
                fillColor: Color(0xFF68080A),
                filled: true,
                
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFF68080A)),
                    borderRadius: BorderRadius.all(Radius.circular(15.0),),),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFA8070A)),
                    borderRadius: BorderRadius.all(Radius.circular(15.0),),),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Color(0xFFA8070A), width: 10.0),
                    borderRadius: BorderRadius.all(Radius.circular(15.0))),
                hintText: this.hintText,
                hintStyle: TextStyle(color: Color(0x5FFFFFFF))))));
  }
}
