import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class SecondScreen extends StatelessWidget {
  final String nameText;
  final String famText;
  final String pinText;

  SecondScreen(
      {@required this.nameText,
      @required this.famText,
      @required this.pinText});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.all(0.0),
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: Image.asset('res/bg 1.jpg').image, fit: BoxFit.cover),
            ),
            child: SingleChildScrollView(
                child: Column(children: <Widget>[
                  SizedBox(height: 50),
              Image.asset('res/logo1.png'),
              SizedBox(height: 30.0),
              QrImage(
                data: nameText + famText + pinText,
                version: 2,
                size: 280.0,
              ),
              SizedBox(height: 20.0),
              ButtonTheme(
                minWidth: 194.0,
                height: 33.0,
                child: ClipRRect(
                    child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 33.0,
                    width: 194.0,
                    padding: const EdgeInsets.all(0.0),
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        gradient: LinearGradient(colors: <Color>[
                          Color(0xFFA8070A),
                          Color(0xFF55080A),
                        ])),
                    child: Text(
                      'Перерегистрация',
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: 14.0, color: Color(0x5FFFFFFF)),
                    ),
                  ),
                )),
              ),
            ]))));
  }
}
