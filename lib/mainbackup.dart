import 'package:flutter/material.dart';
import 'SecondScreen.dart';


class MainScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      //appBar: new AppBar(title: new Text('Flutter.su - Форма ввода')),
      body: new MyForm()
    );
  }
}


class MyForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyFormState();
}

class MyFormState extends State {

  TextEditingController familyTextController = TextEditingController();
  TextEditingController nameTextController = TextEditingController();
  TextEditingController pinTextController = TextEditingController();
  Widget build(BuildContext context) {
    return Container(padding: EdgeInsets.all(50.0), 
      // decoration: BoxDecoration(
      //   image: DecorationImage(image: Image.network('https://flutter.su/data/4e405c78a41d983fe87757c0c7e3885b.jpg').image, fit: BoxFit.cover),
      //   borderRadius: BorderRadius.circular(15)
      // ),
      child: new SingleChildScrollView(child: 
      new Form( child: new Column(children: <Widget>[
      //Image.asset('res/logo1.png'),
      new Text('Введите имя и фамилию:', style: TextStyle(fontSize: 14.0)),      
      new SizedBox(height: 20.0),      
      new TextField(     
        controller: nameTextController,    
        obscureText: false,
        textAlign: TextAlign.center,
        decoration:  InputDecoration(
          hoverColor: Color(0xFF68080A),          
          fillColor: Color(0xFF68080A),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),          
          hintText: 'Имя', 
      ),
      ),
      new SizedBox(height: 20.0),
      new TextField(
        controller: familyTextController,
        obscureText: false,
        textAlign: TextAlign.center,
        decoration: InputDecoration(          
          border: OutlineInputBorder(            
            borderRadius: BorderRadius.all(Radius.circular(10.0))
          ),          
          hintText: 'Фамилия', 
        )
      ),
      
      new SizedBox(height: 40.0),
      new Text('Придумайте пин-код', style: TextStyle(fontSize: 14.0)),     
      new SizedBox(height: 20.0),
      new ConstrainedBox(
        constraints: BoxConstraints(maxWidth: 200),
        child: new TextField(
          controller: pinTextController,
          obscureText: true,
          textAlign: TextAlign.center,
          decoration: InputDecoration(  
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
            hintText: '0000', 
          )
        ),
      ),
      new SizedBox(height: 30.0),


      ButtonTheme(
        minWidth: 194.0,
        height: 33.0,
        child:
        new FlatButton(onPressed: (){          
          String fam = familyTextController.text;
          String name = nameTextController.text;
          String pin = pinTextController.text;
          print(fam);
          print(name);
          print(pin);
          
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SecondScreen(famText: fam, nameText: name, pinText: pin))
          );
        }, child: Text('Получить QR код', style: TextStyle(fontSize: 14.0)),
      
      color: Color(0xFF68080A), 
      textColor: Colors.white
      ),
      )
      
    ],), ))
    );
  }
}

void main() => runApp(
    new MaterialApp(
      // initialRoute: '/',
      // routes: {
      //   '/':(BuildContext context) => MainScreen(),
      //   //'/second':(BuildContext context) => SecondScreen()
      // },
      home: MainScreen(),
      debugShowCheckedModeBanner: false
      
      )
    
);